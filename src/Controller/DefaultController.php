<?php

namespace App\Controller;

use App\Entity\Comentarios;
use App\Entity\Formulario;
use App\Model\ActionFormType;
use App\Repository\ComentariosRepository;
use Doctrine\DBAL\Schema\Column;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PhpParser\Node\Expr\Cast\Object_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use function PHPSTORM_META\map;

class DefaultController extends AbstractController{
    public function index(){
        return new Response("Este es mi primer controlador");
    }

    // public function saludos(){
    //     return new Response("<h1>Esto es un saludo</h1>");
    // }

    /**
     * @Route("/peliculas/{id}", methods={"POST"})
     */
    // public function peliculas($id){
    //     return new Response("Listado de peliculas $id");
    // }

    /**
     * @Route("/peliculas/{id}", methods={"GET"})
     */
    // public function getPeliculas($id){
    //     return new Response("Listado de peliculas $id");
    // }

    /**
     * @Route("/calcula/{valor}", methods={"GET"})
     */
    // public function palabraNumero($valor){
    //     if(is_numeric($valor)){
    //         if($valor % 2 ==0){
    //             return new Response("Es par");
    //         }else{
    //             return new Response("Es impar");
    //         }
    //     }else{
    //         $cad = "";
    //         for($i = 0; $i < strlen($valor); $i++){
    //             if($i % 2 == 0){
    //                 $cad .= strtolower($valor[$i]);
                    
    //             }else{
    //                 $cad .= strtoupper($valor[$i]);
    //             }
    //         }
    //         return new Response($cad);
    //     }
    // }

    /**
     * @Route("/maleteo", name="maleteo")
     */
    public function maleteo(Request $request, EntityManagerInterface $em){
        $form = $this->createForm(ActionFormType::class);

        $form->handleRequest($request);


        if($form->isSubmitted() && $form->isValid()){
            //$submited = $form->getData();
            var_dump($_POST);

            $formulario = new Formulario();
            $formulario->setNombre($_POST['action_form']['Nombre']);
            $formulario->setEmail($_POST['action_form']['Email']);
            $formulario->setCiudad($_POST['action_form']['Ciudad']);

            $em->persist($formulario);
            $em->flush();

            return new JsonResponse(Response::HTTP_OK);
            //return $this->redirectToRoute("maleteo");
            //return $this->redirectToRoute('submited');
        }

        $repositorio = $em->getRepository(Comentarios::class);
        //$comen = $repositorio->findAll();
        $comen = $repositorio->findLastThree(); //Da error pero funciona

        $comentarios = $comen;

        return $this->render("maleteo.html.twig", [
            'comentarios'=> $comentarios,
            'formulario' =>$form->createView()
            ]);
    }

    /**
     * @Route("/comentarios")
     */
    // public function comentarios(EntityManagerInterface $em){
    //     $com = new Comentarios();
    //     $com->setTexto('Tras el enorme exito internacional de su primera colaboracion, Bailar, que gano un galardon en los Premios');
    //     $com->setPersona('Sergio Garnacho');
    //     $com->setCiudad('Tetuan, Madrid');

    //     $em->persist($com);
    //     $em->flush();
    //     return $this->render("comentarios.html.twig");
    // }

    /**
     * @Route("/submited" , name="submited")
     */
    public function formSubmited(){
        return $this->render("form-success.html.twig");
    }

    /**
     * @Route("/listado" , name="listado")
     */
    public function listForm(EntityManagerInterface $em){
        $rep = $em->getRepository(Formulario::class);
        $f = $rep->findAll();
        
        return $this->render("list-form-data.html.twig", 
        [
            'listado' => $f
        ]);
    }

    /**
     * @Route("/listado/deleted/{id}" , name="borrado")
     */
    public function deletedFromList(EntityManagerInterface $em, Formulario $formulario){
        //$rep = $em->getRepository(Formulario::class);
        //$borrado = $rep->findBy($id);

        $em->remove($formulario);
        $em->flush();
        
        return $this->redirectToRoute("listado");
    }

    /**
     * @Route("/listado/edit/{id}" , name="editado")
     */
    public function editFromList(Formulario $formulario,Request $request,EntityManagerInterface $em, $id){
        
        $form = $this->createForm(ActionFormType::class, $formulario);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $edited = $form->getData();

            $em->persist($edited);
            $em->flush();


            //return $this->redirectToRoute("maleteo");
            return $this->redirectToRoute('listado');
        }
        
        return $this->render("edit-form.html.twig", [
            'formulario' => $form->createView()
        ]);
    }
}