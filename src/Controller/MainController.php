<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\Usuario;

class MainController extends AbstractController{
    /**
     * @Route("/contacto")
     */
    public function contacto(){
        $respuesta = new Response("Ayyyyy lmao");

        return $respuesta;
    }

    /**
     * @Route("/saluda/{nombre}/{apellidos}", methods={"POST", "GET"})
     */
    public function saluda($nombre, $apellidos){
        $variables =
        [
            'nombre' => $nombre,
            'apellidos' => $apellidos
        ];

        return $this->render("index.html.twig", $variables);
    }

    /**
     * @Route("/login/{nombre}/{registrado}", methods={"POST", "GET"})
     */
    public function login($nombre, $registrado){
        
        $user = new Usuario("Manuel", true);

        $variables = [
            'usuario' => $user
        ];

        return $this->render("index.html.twig", $variables);
    }
}