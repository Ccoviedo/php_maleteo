<?php

namespace App\Model;

class Usuario{

    private $nombre;
    public $registrado;

    public function __construct($nombre, $registrado)
    {
        $this->registrado = $registrado;
        $this->nombre = $nombre;
    }

    /**
     * Get the value of registrado
     */ 
    public function getRegistrado()
    {
        return $this->registrado;
    }

    /**
     * Set the value of registrado
     *
     * @return  self
     */ 
    public function setRegistrado($registrado)
    {
        $this->registrado = $registrado;

        return $this;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }
}