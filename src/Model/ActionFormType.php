<?php

namespace App\Model;

use App\Entity\Formulario;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActionFormType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('Nombre', TextType::class ,[
            'attr' => [
                'placeholder' => 'Vicent Chase'
            ],
        ]);

        $builder->add('Email', EmailType::class ,[
            'attr' => [
                'placeholder' => 'vincent@mga.com'
            ],
        ]);

        // $builder->add('Horario_Preferido', EmailType::class ,[
        //     'attr' => [
        //         'placeholder' => 'vincent@mga.com'
        //     ],
        // ]);

        $builder->add('Ciudad', ChoiceType::class, [
            'choices'=> [
                'Madrid' => 'Madrid',
                'Bilbao' => 'Bilbao',
                'Sevilla' => 'Sevilla'
            ]
        ]);
        
        $builder->add('Terminos', CheckboxType::class, [
            'mapped' => false,
            'label' => 'He leido y acepto la politica de privacidad'
        ]);

        $builder->add("Enviar",SubmitType::class, [
            'attr' => [
                'class' => 'btnEnviar'
            ],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Formulario::class]);
    }
}